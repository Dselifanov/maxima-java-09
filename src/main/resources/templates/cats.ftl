<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>${title}</title>
</head>
<body>
<table>
    <tr>
        <th>ID</th>
        <th>${header_name}</th>
    </tr>
    <#list cats as cat>
        <tr>
            <td>${cat.id}</td>
            <td>${cat.name}</td>
        </tr>
    </#list>
</table>
<br/><br/>

<form method="post" action="cat">
    <h1>Add cat</h1>
    <p><input name="id" placeholder="Cat id ..."/></p>

    <p><label><input type="radio" name="name" value="Murzik"/>Murzik</label></p>
    <p><label><input type="radio" name="name" value="Barsik"/>Barsik</label></p>
    <p><label><input type="radio" name="name" value="Murka"/>Murka</label></p>

    <br/><br/>

    <p><input type="submit" value="Add cat"/></p>
</form>

</body>
</html>