package org.example.maximajava1101;

import org.example.maximajava1101.model.Cat;
import org.example.maximajava1101.repository.CatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MaximaJava1101Application implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(MaximaJava1101Application.class, args);
	}

	@Autowired private CatRepository repo;

	@Override
	public void run(String... args) throws Exception {
		repo.save(new Cat("Murzik", 8, true));
		repo.save(new Cat("Barsik", 3,false));
		repo.save(new Cat("Murka", 5,false));
		System.out.println("Hello");
	}
}
