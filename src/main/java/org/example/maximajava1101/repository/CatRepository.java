package org.example.maximajava1101.repository;


import org.example.maximajava1101.model.Cat;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

@Repository
public interface CatRepository extends JpaRepository<Cat, Long> {


}
