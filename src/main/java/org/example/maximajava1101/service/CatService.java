package org.example.maximajava1101.service;


import org.example.maximajava1101.model.Cat;
import org.example.maximajava1101.repository.CatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CatService {
    @Autowired
    private CatRepository repo;



    public List<Cat> getAllCats(){
    return repo.findAll();
    }

    public void createCat(Cat cat){
        repo.save(cat);
    }
}
